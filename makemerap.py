#!/usr/bin/env python3
import re
import os
import csv
import sys
from collections import defaultdict

file_path = sys.argv[1] if len(sys.argv) > 1 else 'dictionaries/psychometric.csv'

reader = csv.reader(open(file_path, encoding='utf8'))
delimiter = re.compile(r'[(, )(; )]+')

# Extract all words
words = set()
for line in reader:
  word, meaning = line
  meanings = delimiter.split(meaning[3:-4])
  words.add(word.split(' ')[-1].strip())  # In case 'word' is a statement, take the last word.
  words.update(map(lambda word: word.split(' ')[-1].strip(),meanings))

# Create word groups
groups = defaultdict(set)
for word in words:
  if len(word) >= 2:
    groups[word[-2:]].add(word)

# Create arcs
# dict_arcs is in the format of {a: [b, c, ...]} == an arc from a to b and from a to c
dict_arcs = defaultdict(list)
for group in groups.values():
  # arcs is in the format of (a, b) == an arc from a to b
  group = list(group)
  arcs = list(zip(group, group[1:] + group[:1])) + list(zip(group, group[2:] + group[:2]))
  for k, v in arcs:
    dict_arcs[k].append(v)

# Write a csv!
def to_csv(file_path=file_path, csv_path=None):
  if csv_path is None:
    file_name = os.path.split(file_path)[-1]
    csv_path = os.path.splitext(file_name)[0] + '.csv'
  with open(csv_path, "w") as file_obj:
    for word, rhymes in dict_arcs.items():
      file_obj.write(';'.join((word, ', '.join([rhyme for rhyme in rhymes]))))
      file_obj.write('\n')

if '__main__' == __name__:
  to_csv()

